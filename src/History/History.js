import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';


function History() {
    let [history, setHistory] = useState([]);
    useEffect(() => {

        console.log("im in useEffect!");
        let url = 'http://localhost:8080/parking/history';
        let param = { method: 'GET' };
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            setHistory(json);
        }).catch((err) => {
            console.log(err);
        });
    }, []);



    return (
        
        <div className='container'>
            <div className="row">
                <div className="col-md-8">
                 
                    <div id="list">
                        <Table striped bordered hover variant="dark">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>EVENT</th>
                                    <th>TIMESTAMP</th>
                                    <th>License Plate</th>
                                </tr>
                            </thead>
                            <tbody>
                                {history.map((f, no) => (
                                    <tr key={f.Id}>
                                        <td>{no + 1}.</td>
                                        <td>{f.event}</td>
                                        <td>{f.timestamp}</td>
                                        <td>{f.vehicle.licensePlate}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default History;