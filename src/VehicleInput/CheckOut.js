// import { useEffect, useState } from "react";


// function CheckOut() {
//     let [film, setFilm] = useState([]);
//     let [filmId, setFilmId] = useState(1);

//     useEffect(() => {
//         // run one time..just like constructor
//         console.log("im in useEffect!");
//         let url = 'http://localhost:8080/parking/find-all';
//         let param = { method: 'GET' };
//         fetch(url, param).then((data) => {
//             return data.json();
//         }).then((json) => {
//             console.log(json);
//             setFilm(json);
//         }).catch((err) => {
//             console.log(err);
//         });
//     }, []);

//     const doClick = (id) => {
//         setFilmId(id);
//         console.log("film id = ", vehcileId);
//     }

//     return (
//         <div>
//             <div className="row">
//                 <div className="col-md-8">
//                     <h1>Film List</h1>
//                     <div id="search-list">
//                         <table className="table table-bordered table-striped table-hover">
//                             <thead>
//                                 <tr>
//                                     <th>Vehicle ID</th>
//                                     <th>License Plate</th>
//                                 </tr>
//                             </thead>
//                             <tbody>
//                                 {film.map((f, no) => (
//                                     <tr key={f.vehicelId}>
//                                         <td>{no + 1}.</td>
//                                         <td><a href="##" onClick={() => doClick(f.vehcileId)}>{f.licensePlate}</a></td>
//                                         <td><button  classname = 'btn btn-danger'onClick={() => {
//                                             const url = `http://localhost:8080/parking/unpark/${f.vehicleId}`;
//                                             fetch(url, { method: 'POST' })
//                                                 .then((data) => {
//                                                     console.log(data);
//                                                     // Refresh the film list after check-out
//                                                     setFilm([]);
//                                                 })
//                                                 .catch((error) => {
//                                                     console.log(error);
//                                                 });
//                                         }}>Check Out</button></td>

//                                     </tr>
//                                 ))}
//                             </tbody>
//                         </table>
//                     </div>
//                 </div>

//             </div>

//         </div>
//     );
// }

// export default CheckOut;
import { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';

function CheckOut() {
    let [film, setFilm] = useState([]);
    let [licensePlate, setLicensePlate] = useState('');
    let [filmId, setFilmId] = useState(1);

    useEffect(() => {
        console.log("im in useEffect!");
        let url = 'http://localhost:8080/parking/find-all';
        let param = { method: 'GET'};
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            setFilm(json);
        }).catch((err) => {
            console.log(err);
        });
    }, []);


    const handleCheckOut = (licensePlate) => {
        let url = `http://localhost:8080/parking/unpark?licensePlate=${licensePlate}`;
        let param = { method: 'POST'};
       
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            setLicensePlate(json);
        }).catch((err) => {
            console.log(err);
        });
    }

    return (
        <div className='row justify-content-center align-items-center'>
            <div className='row justify-content-center align-items-center'>
                <div className="col-md-8">
                    <h1>Vehicle List</h1>
                    <div>
                        <table className="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Vehicle ID</th>
                                    <th>License Plate</th>
                                    <th>Check Out</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                { film.map((f, no) => (
                                <tr key={f.vehicleId}>
                                    <td>{ no + 1}.</td>
                                    <td>{ f.licensePlate }</td>
                                    
                                    <td><Button  variant="danger"onClick={() => handleCheckOut(f.licensePlate)}>Check Out</Button></td>
                                </tr>
                                )) }
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
            
        </div>
    );
}

export default CheckOut;
