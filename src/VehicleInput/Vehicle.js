// import { useState, useEffect } from 'react';
// import { Select } from 'antd';
// import './Vehicle.css';

// function Vehicle(props) {
//     // let [isShow, setIsShow] = useState(false);
//     let [licensePlate, setLicensePlate] = useState('');
//     let [type, setType] = useState('');
//     // const options = [
//     //     { value: 'car', label: 'CAR' },
//     //     { value: 'bike', label: 'BIKE' },

//     //   ]

//     function doSave() {
//         let url = 'http://localhost:8080/parking/park';
//         let data = { licensePlate, type };
//         let param = {
//             method: 'POST',
//             headers: { 'Content-Type': 'application/json' },
//             body: JSON.stringify(data)
//         };
//         fetch(url, param)
//             .then(data => data.json())
//             .then(json => {

//                 console.log(json);
//                 alert('Parked');
//             });

//     };



//     console.log(props);

//     return (
//         <div className='row justify-content-center align-items-center'>
//             <div id='myform'>
//                 <h4>Vehicle Entry</h4>
//                 <div className='row mb-2'>
//                     <div className='col-md-12'>
//                         <label>License Plate</label>
//                         <input type='text' className='form-control'
//                             value={licensePlate} onChange={(event) => setLicensePlate(event.target.value)} placeholder="Enter License Plate"/>
//                     </div>
//                 </div>
//                 <div className='row mb-2'>
//                     <div className='col-md-12'>
//                         <label>Vehicle Type</label>
//                         <select className='form-control' value={type} onChange={(event) => setType(event.target.value)}>
//                             <option value="NONE">NONE</option>
//                             <option value="BIKE">BIKE</option>
//                             <option value="CAR">CAR</option>
//                         </select>
//                         {/* <textarea className='form-control' value={type}
//                                 onChange={(event) => setType(event.target.value)}></textarea> */}
//                     </div>
//                 </div>
//                 <div className='row'>
//                     <div className='col-md-12'>
//                         <button className='btn btn-primary' onClick={doSave}>Submit</button>
//                         {/* <button onClick={() => setIsShow(false)} className='btn btn-warning'>Close</button> */}
//                     </div>
//                 </div>

//             </div>
//         </div>
//     );


// }


// export default Vehicle;
import { useState } from 'react';
import { Select } from 'antd';
import './Vehicle.css';

function Vehicle(props) {
  const [licensePlate, setLicensePlate] = useState('');
  const [type, setType] = useState('');
  const [licensePlateError, setLicensePlateError] = useState('');
  const [typeError, setTypeError] = useState('');

  function doSave() {
    let url = 'http://localhost:8080/parking/park';
    let data = { licensePlate, type };
    let param = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    };
    fetch(url, param)
      .then(data => data.json())
      .then(json => {
        console.log(json);
        alert('Parked');
      });
  };

  function validate() {
    let licensePlateError = '';
    let typeError = '';

    if (!licensePlate) {
      licensePlateError = 'License plate is required';
    }

    if (type === 'NONE') {
      typeError = 'Vehicle type is required';
    }

    setLicensePlateError(licensePlateError);
    setTypeError(typeError);

    return !licensePlateError && !typeError;
  }

  function handleSubmit(event) {
    event.preventDefault();
    const isValid = validate();

    if (isValid) {
      doSave();
    }
  }

  return (
    <div className='row justify-content-center align-items-center'>
      <div id='myform'>
        <h4>Vehicle Entry</h4>
        <form onSubmit={handleSubmit}>
          <div className='row mb-2'>
            <div className='col-md-12'>
              <label>License Plate</label>
              <input
                type='text'
                className={`form-control ${licensePlateError ? 'is-invalid' : ''}`}
                value={licensePlate}
                onChange={(event) => setLicensePlate(event.target.value)}
                placeholder="Enter License Plate"
              />
              <div className="invalid-feedback">{licensePlateError}</div>
            </div>
          </div>
          <div className='row mb-2'>
            <div className='col-md-12'>
              <label>Vehicle Type</label>
              <select
                className={`form-control ${typeError ? 'is-invalid' : ''}`}
                value={type}
                onChange={(event) => setType(event.target.value)}
              >
                <option value="NONE">NONE</option>
                <option value="BIKE">BIKE</option>
                <option value="CAR">CAR</option>
              </select>
              <div className="invalid-feedback">{typeError}</div>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-12'>
              <button className='btn btn-primary' type="submit">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Vehicle;


