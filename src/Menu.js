import { Link } from "react-router-dom";
function Menu() {
    return (
        <div className="container px-4 px-lg-5">
            <div className="navbar">
                <img src ="https://thumbs.dreamstime.com/b/black-cars-parking-lot-parking-icon-logo-black-cars-parking-lot-parking-icon-logo-white-130998396.jpg" alt="Logo" width="50" height="50" className="d-inline-block align-text-top"/>
                <h1>Car Parking</h1>
                <div className="links">
                    <Link to="/">Home</Link>
                    <Link to="/history">History Page</Link>
                    <Link to="/checkout">Vehicle Checkout</Link>
                </div>
            </div>
        </div>
    );
}

export default Menu;