import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { FileOutlined, PieChartOutlined, UserOutlined,DesktopOutlined, TeamOutlined  } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { CarouselItem } from 'react-bootstrap';
// import { useState } from 'react';
// function HomePage() {
//     const [parkings, setParkings] = useState([]);


//     useEffect(()=>{
//         fetch('http://localhost:8080/parking/available')
//         .then(response => response.json())
//         .then(data => setParkings(data))
//         .catch(error => console.log(error));
//     }, []);
    

//     return (

//         <div>
//             <h2>SMALL</h2>
//             <Row xs={1} md={2} className="g-4">
                
//                     <div>
//                         <Col>
//                             <Card>
//                                 <Card.Body >
//                                     <Card.Title>SMALL</Card.Title>
//                                     <Card.Text>
//                                         Ticket Issue: <br />
//                                         Client Name :<br />
//                                         Created At: <br />
//                                         {/* Assigned To : {ticket.assignedUser.userName} */}

//                                     </Card.Text>
//                                 </Card.Body>
//                             </Card>
//                         </Col>
//                     </div>
             
//             </Row>
//         </div>

//     );
// }

const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem('Vehicle Parking', '1', <CarouselItem/>),
  getItem('History', '2', <CarouselItem />),
];
const HomePage = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: '100vh',
      }}
    >
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div
          style={{
            height: 32,
            margin: 16,
            background: 'rgba(255, 255, 255, 0.2)',
          }}
        />
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        />
        <Content
          style={{
            margin: '0 16px',
          }}
        >
          <Breadcrumb
            style={{
              margin: '16px 0',
            }}
          >
            <Breadcrumb.Item>User</Breadcrumb.Item>
            
          </Breadcrumb>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            <History/>.
          </div>
        </Content>
        {/* <Footer
          style={{
            textAlign: 'center',
          }}
        >
        </Footer> */}
      </Layout>
    </Layout>
  );
};



export default HomePage;