import logo from './logo.svg';
import { BrowserRouter as Router, Routes, Route, Form }from 'react-router-dom';
import './App.css';
import HomePage from './Homepage/Homepage';
import History from './History/History';
import Vehicle from './VehicleInput/Vehicle';
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { FileOutlined, PieChartOutlined, UserOutlined,DesktopOutlined, TeamOutlined  } from '@ant-design/icons';
// import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { CarouselItem } from 'react-bootstrap';
import CheckOut from './VehicleInput/CheckOut';
import Menu from './Menu';

function App() {
  return (
      <Router>
           <Menu />
         <Routes>
      

            <Route path='/' element={<Vehicle />}></Route>
            {/* <Route path='/department_user/add-solution' element={<AddSolution />}></Route> */}
            {/* <Route path='/homepage' element={<HomePage />}></Route> */}
            <Route path='/history' element={<History />}></Route>
            <Route path='/checkout' element={<CheckOut />}></Route>
           {/* <Route path='/vehicle' element={<Vehicle />}></Route>  */}
            {/* <Route path='/department_user/solution-detail/:id' element={<SolutionDetail />}></Route>
            <Route path='/department_user/add-solution/:id' element={<AddSolution />}></Route>
            <Route path='/admin/tickets/closed' element={<AdminHome />}></Route>
            <Route path='/admin/add-rating' element={<AddRating />}></Route>
            <Route path='/admin/edit-rating' element={<EditRating />}></Route>
            <Route path='/admin/closed-tickets/:id' element={<AddRating />}></Route>
            <Route path='/admin/reports' element={<Report />}></Route>
          <Route path='/secretarial/ticketlist' element={<TicketList />}></Route>
            <Route path='/secretarial/createticket' element={<CreateTicket />}></Route> */}
      
      
      </Routes>
    </Router>
  );
}
// const { Header, Content, Footer, Sider } = Layout;
// function getItem(label, key, icon, children) {
//   return {
//     key,
//     icon,
//     children,
//     label,
//   };
// }
// const items = [
//   getItem('Vehicle Parking', '1', <Vehicle/>),
//   getItem('History', '2', <CarouselItem />),
// ];
// const App = () => {
//   const [collapsed, setCollapsed] = useState(false);
//   const {
//     token: { colorBgContainer },
//   } = theme.useToken();
//   return (
//     <Layout
//       style={{
//         minHeight: '100vh',
//       }}
//     >
//       <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
//         <div
//           style={{
//             height: 32,
//             margin: 16,
//             background: 'rgba(255, 255, 255, 0.2)',
//           }}
//         />
//         <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
//       </Sider>
//       <Layout className="site-layout">
//         <Header
//           style={{
//             padding: 0,
//             background: colorBgContainer,
//           }}
//         />
//         <Content
//           style={{
//             margin: '0 16px',
//           }}
//         >
//           <Breadcrumb
//             style={{
//               margin: '16px 0',
//             }}
//           >
            

            
//           </Breadcrumb>
//           <div
//             style={{
//               padding: 24,
//               minHeight: 360,
//               background: colorBgContainer,
//             }}
//           >
//             <History/>
            
//           </div>
//         </Content>
//         {/* <Footer
//           style={{
//             textAlign: 'center',
//           }}
//         >
//         </Footer> */}
//       </Layout>
//     </Layout>
//   );
// };


export default App;
